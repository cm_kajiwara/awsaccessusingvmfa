﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Livet;
using Amazon.S3.Model;
using Amazon.Runtime;
using System.Threading.Tasks;

namespace AWS_S3_Console.Models
{
    public class AWSS3Buckets : NotificationObject
    {
        /*
         * NotificationObjectはプロパティ変更通知の仕組みを実装したオブジェクトです。
         */
       
        public String AWSAccessKey { get; set; }

        public String AWSSecretKey { get; set; }

        public SessionAWSCredentials AWSCredentials { get; set; }


        public ObservableSynchronizedCollection<S3Bucket> Buckets { get; set; }

        public System.Threading.CancellationToken cancel { get; set; }

        public async Task<ListBucketsResponse> getBucketList()
        {
            using (var client = Amazon.AWSClientFactory.CreateAmazonS3Client(this.AWSCredentials,Amazon.RegionEndpoint.APNortheast1))
            {
                try
                {
                    return  await client.ListBucketsAsync(new ListBucketsRequest(), cancel);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
    }
}
