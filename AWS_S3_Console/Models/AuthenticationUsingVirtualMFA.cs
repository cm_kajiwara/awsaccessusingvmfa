﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Livet;
using Amazon.IdentityManagement;
using Amazon.IdentityManagement.Model;
using Amazon.SecurityToken;
using Amazon.SecurityToken.Model;
using Amazon.Runtime;
using System.Threading.Tasks;

namespace AWS_S3_Console.Models
{
    public class AuthenticationUsingVirtualMFA : NotificationObject
    {
        /*
         * NotificationObjectはプロパティ変更通知の仕組みを実装したオブジェクトです。
         */

        public String AccessKey { get; set; }
        public String SecretKey { get; set; }
        public String SerialKey { get; set; }
        public String TokenCode { get; set; }
        public System.Threading.CancellationToken cancel { get; set; }

        
        

        public async Task<ListVirtualMFADevicesResponse> getVirtualMFADeviceListResponse()
        {
            using (var client = new AmazonIdentityManagementServiceClient(this.AccessKey, this.SecretKey))
            {
                var listVirtualMFADeviceSerialKeys = new ObservableSynchronizedCollection<string>();
                return  await client.ListVirtualMFADevicesAsync(new ListVirtualMFADevicesRequest(), cancel);
                //foreach (var device in virtualMFADeviceResponse.VirtualMFADevices)
                //{
                //    listVirtualMFADeviceSerialKeys.Add(device.SerialNumber);
                //}
                //return listVirtualMFADeviceSerialKeys;
            }
        }

        public async Task<SessionAWSCredentials> createSecurityCredentials()
        {
            var secreq = new GetSessionTokenRequest();
            secreq.SerialNumber = this.SerialKey;
            secreq.TokenCode = this.TokenCode;
            secreq.DurationSeconds = 3600;

            var stsclient = new AmazonSecurityTokenServiceClient(this.AccessKey, this.SecretKey);
            var stsResult = await stsclient.GetSessionTokenAsync(secreq,cancel);
            var awsCre = stsResult.Credentials;
            return new SessionAWSCredentials(awsCre.AccessKeyId, awsCre.SecretAccessKey, awsCre.SessionToken);
        }

    }
}
