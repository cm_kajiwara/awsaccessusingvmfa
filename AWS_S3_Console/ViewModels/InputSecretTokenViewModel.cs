﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

using Livet;
using Livet.Commands;
using Livet.Messaging;
using Livet.Messaging.IO;
using Livet.EventListeners;
using Livet.Messaging.Windows;

using AWS_S3_Console.Models;
using System.Windows;
using System.Threading.Tasks;

namespace AWS_S3_Console.ViewModels
{
    public class InputSecretTokenViewModel : ViewModel
    {
        /* コマンド、プロパティの定義にはそれぞれ 
         * 
         *  lvcom   : ViewModelCommand
         *  lvcomn  : ViewModelCommand(CanExecute無)
         *  llcom   : ListenerCommand(パラメータ有のコマンド)
         *  llcomn  : ListenerCommand(パラメータ有のコマンド・CanExecute無)
         *  lprop   : 変更通知プロパティ(.NET4.5ではlpropn)
         *  
         * を使用してください。
         * 
         * Modelが十分にリッチであるならコマンドにこだわる必要はありません。
         * View側のコードビハインドを使用しないMVVMパターンの実装を行う場合でも、ViewModelにメソッドを定義し、
         * LivetCallMethodActionなどから直接メソッドを呼び出してください。
         * 
         * ViewModelのコマンドを呼び出せるLivetのすべてのビヘイビア・トリガー・アクションは
         * 同様に直接ViewModelのメソッドを呼び出し可能です。
         */

        /* ViewModelからViewを操作したい場合は、View側のコードビハインド無で処理を行いたい場合は
         * Messengerプロパティからメッセージ(各種InteractionMessage)を発信する事を検討してください。
         */

        /* Modelからの変更通知などの各種イベントを受け取る場合は、PropertyChangedEventListenerや
         * CollectionChangedEventListenerを使うと便利です。各種ListenerはViewModelに定義されている
         * CompositeDisposableプロパティ(LivetCompositeDisposable型)に格納しておく事でイベント解放を容易に行えます。
         * 
         * ReactiveExtensionsなどを併用する場合は、ReactiveExtensionsのCompositeDisposableを
         * ViewModelのCompositeDisposableプロパティに格納しておくのを推奨します。
         * 
         * LivetのWindowテンプレートではViewのウィンドウが閉じる際にDataContextDisposeActionが動作するようになっており、
         * ViewModelのDisposeが呼ばれCompositeDisposableプロパティに格納されたすべてのIDisposable型のインスタンスが解放されます。
         * 
         * ViewModelを使いまわしたい時などは、ViewからDataContextDisposeActionを取り除くか、発動のタイミングをずらす事で対応可能です。
         */

        /* UIDispatcherを操作する場合は、DispatcherHelperのメソッドを操作してください。
         * UIDispatcher自体はApp.xaml.csでインスタンスを確保してあります。
         * 
         * LivetのViewModelではプロパティ変更通知(RaisePropertyChanged)やDispatcherCollectionを使ったコレクション変更通知は
         * 自動的にUIDispatcher上での通知に変換されます。変更通知に際してUIDispatcherを操作する必要はありません。
         */

        public InputSecretTokenViewModel()
        {
            authenticationUsingVirtualMFA = new AuthenticationUsingVirtualMFA();
        }
        public AuthenticationUsingVirtualMFA authenticationUsingVirtualMFA {get;set;}

        
        public void Initialize()
        {
        }


        #region SecretToken変更通知プロパティ
        private string _SecretToken;

        public string SecretToken
        {
            get
            { return _SecretToken; }
            set
            { 
                if (_SecretToken == value)
                    return;
                _SecretToken = value;
                RaisePropertyChanged(()=> SecretToken);
                CreateSecurityCredentialCommand.RaiseCanExecuteChanged();
            }
        }
        #endregion


        #region SerialKey変更通知プロパティ
        private string _SerialKey;

        public string SerialKey
        {
            get
            { return _SerialKey; }
            set
            { 
                if (_SerialKey == value)
                    return;
                _SerialKey = value;
                RaisePropertyChanged(()=>SerialKey);
                CreateSecurityCredentialCommand.RaiseCanExecuteChanged();
            }
        }
        #endregion


        #region SelectedSeriakLeyIndex変更通知プロパティ
        private int _SelectedSeriakLeyIndex;

        public int SelectedSeriakLeyIndex
        {
            get
            { return _SelectedSeriakLeyIndex; }
            set
            { 
                if (_SelectedSeriakLeyIndex == value)
                    return;
                _SelectedSeriakLeyIndex = value;
                RaisePropertyChanged(()=>SelectedSeriakLeyIndex);
            }
        }
        #endregion


        public ObservableSynchronizedCollection<string> ListVirtualMFADeviceSerialKeys { get; set; }

        #region CreateSecurityCredentialCommand
        private ViewModelCommand _CreateSecurityCredentialCommand;

        public ViewModelCommand CreateSecurityCredentialCommand
        {
            get
            {
                if (_CreateSecurityCredentialCommand == null)
                {
                    _CreateSecurityCredentialCommand = new ViewModelCommand(CreateSecurityCredential, CanCreateSecurityCredential);
                }
                return _CreateSecurityCredentialCommand;
            }
        }

        public bool CanCreateSecurityCredential()
        {
           return !( String.IsNullOrEmpty(this.authenticationUsingVirtualMFA.AccessKey) || 
                     String.IsNullOrEmpty(this.authenticationUsingVirtualMFA.SecretKey) ||
                     String.IsNullOrEmpty(SerialKey) ||
                     String.IsNullOrEmpty(SecretToken));
        }

        public async void CreateSecurityCredential()
        {
            var awsBucketListViewModel = new AWSBucketListViewModel();
            this.authenticationUsingVirtualMFA.TokenCode = this.SecretToken;
            this.authenticationUsingVirtualMFA.SerialKey = this.SerialKey;
            var sessionCredentials = await this.authenticationUsingVirtualMFA.createSecurityCredentials();
            awsBucketListViewModel.awsS3Buckets.AWSCredentials = sessionCredentials;
            if (await awsBucketListViewModel.getS3Buckets())
            {
                //遷移
                var window = Application.Current.Windows.OfType<Window>().SingleOrDefault((w) => w.IsActive);
                window.Hide();
                Messenger.Raise(new TransitionMessage(awsBucketListViewModel, "OpenBucketLists"));
                window.ShowDialog();
            }
        }
        #endregion


        public async Task<Boolean> getVirtualMFADeviceList()
        {
            this.ListVirtualMFADeviceSerialKeys = new ObservableSynchronizedCollection<string>();
            var  res = await this.authenticationUsingVirtualMFA.getVirtualMFADeviceListResponse();
            if(res.VirtualMFADevices.Count > 0)
            {
                foreach (var item in res.VirtualMFADevices)
                {
                    this.ListVirtualMFADeviceSerialKeys.Add(item.SerialNumber);
                }
            }
            return res.VirtualMFADevices.Count > 0;
        }




    }
}
