﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

using Livet;
using Livet.Commands;
using Livet.Messaging;
using Livet.Messaging.IO;
using Livet.EventListeners;
using Livet.Messaging.Windows;

using AWS_S3_Console.Models;
using System.Windows;

namespace AWS_S3_Console.ViewModels
{
    public class MainWindowViewModel : ViewModel
    {
        /* コマンド、プロパティの定義にはそれぞれ 
         * 
         *  lvcom   : ViewModelCommand
         *  lvcomn  : ViewModelCommand(CanExecute無)
         *  llcom   : ListenerCommand(パラメータ有のコマンド)
         *  llcomn  : ListenerCommand(パラメータ有のコマンド・CanExecute無)
         *  lprop   : 変更通知プロパティ(.NET4.5ではlpropn)
         *  
         * を使用してください。
         * 
         * Modelが十分にリッチであるならコマンドにこだわる必要はありません。
         * View側のコードビハインドを使用しないMVVMパターンの実装を行う場合でも、ViewModelにメソッドを定義し、
         * LivetCallMethodActionなどから直接メソッドを呼び出してください。
         * 
         * ViewModelのコマンドを呼び出せるLivetのすべてのビヘイビア・トリガー・アクションは
         * 同様に直接ViewModelのメソッドを呼び出し可能です。
         */

        /* ViewModelからViewを操作したい場合は、View側のコードビハインド無で処理を行いたい場合は
         * Messengerプロパティからメッセージ(各種InteractionMessage)を発信する事を検討してください。
         */

        /* Modelからの変更通知などの各種イベントを受け取る場合は、PropertyChangedEventListenerや
         * CollectionChangedEventListenerを使うと便利です。各種ListenerはViewModelに定義されている
         * CompositeDisposableプロパティ(LivetCompositeDisposable型)に格納しておく事でイベント解放を容易に行えます。
         * 
         * ReactiveExtensionsなどを併用する場合は、ReactiveExtensionsのCompositeDisposableを
         * ViewModelのCompositeDisposableプロパティに格納しておくのを推奨します。
         * 
         * LivetのWindowテンプレートではViewのウィンドウが閉じる際にDataContextDisposeActionが動作するようになっており、
         * ViewModelのDisposeが呼ばれCompositeDisposableプロパティに格納されたすべてのIDisposable型のインスタンスが解放されます。
         * 
         * ViewModelを使いまわしたい時などは、ViewからDataContextDisposeActionを取り除くか、発動のタイミングをずらす事で対応可能です。
         */

        /* UIDispatcherを操作する場合は、DispatcherHelperのメソッドを操作してください。
         * UIDispatcher自体はApp.xaml.csでインスタンスを確保してあります。
         * 
         * LivetのViewModelではプロパティ変更通知(RaisePropertyChanged)やDispatcherCollectionを使ったコレクション変更通知は
         * 自動的にUIDispatcher上での通知に変換されます。変更通知に際してUIDispatcherを操作する必要はありません。
         */

        public void Initialize()
        {
            inputSecretTokenViewModel = new InputSecretTokenViewModel();
        }

        public InputSecretTokenViewModel inputSecretTokenViewModel { get; set; }

        #region AccessKey変更通知プロパティ
        private string _AccessKey;

        public string AccessKey
        {
            get
            { return _AccessKey; }
            set
            { 
                if (_AccessKey == value)
                    return;
                _AccessKey = value;
                RaisePropertyChanged(()=>AccessKey);
                this.ListMFASerialKeyCommand.RaiseCanExecuteChanged();
            }
        }
        #endregion


        #region SecretKey変更通知プロパティ
        private string _SecretKey;

        public string SecretKey
        {
            get
            { return _SecretKey; }
            set
            { 
                if (_SecretKey == value)
                    return;
                _SecretKey = value;
                RaisePropertyChanged(()=>SecretKey);
                this.ListMFASerialKeyCommand.RaiseCanExecuteChanged();
            }
        }
        #endregion


        #region ListMFASerialKeyCommand
        private ViewModelCommand _ListMFASerialKeyCommand;

        public ViewModelCommand ListMFASerialKeyCommand
        {
            get
            {
                if (_ListMFASerialKeyCommand == null)
                {
                    _ListMFASerialKeyCommand = new ViewModelCommand(ListMFASerialKey, CanListMFASerialKey);
                }
                return _ListMFASerialKeyCommand;
            }
        }

        public bool CanListMFASerialKey()
        {
            if (String.IsNullOrEmpty(this.AccessKey) || String.IsNullOrEmpty(this.SecretKey))
                return false;
            return true;
        }

        public async void ListMFASerialKey()
        {
            this.inputSecretTokenViewModel.authenticationUsingVirtualMFA.AccessKey = this.AccessKey;
            this.inputSecretTokenViewModel.authenticationUsingVirtualMFA.SecretKey = this.SecretKey;
            if (await this.inputSecretTokenViewModel.getVirtualMFADeviceList())
            {
                //遷移
                var window = Application.Current.Windows.OfType<Window>().SingleOrDefault((w) => w.IsActive);
                window.Hide();
                Messenger.Raise(new TransitionMessage(this.inputSecretTokenViewModel, "OpenSerialKeyList"));
                window.ShowDialog();
            }
        }
        #endregion

    }
}
